const assert = require('assert');
const expect = require('chai').expect;
const sinon = require('sinon')
const Score = require('../app/project.js');
const Game = require('../app/game.js');

describe('Game Test', () => {

    context('Play game', () => {

        it('should return the score game', () => {
            //Given
            let score = new Score();
            let frameValue = [5,4];
            sinon.stub(score, 'playFrame').returns(frameValue)//STUB
            let game = new Game(score);
            let expected_result = 90;
            //When
            let result = game.play()
            //Then
            expect(result).to.equal(expected_result);
        })

        it('should call computeScore', () => {
            //Given
            let score = new Score();
            const scoreMock = sinon.mock(score);
            scoreMock.expects('computeScore').once();
            let game = new Game(score);
            //When
            game.play()
            //Then
            scoreMock.verify()
        })

        it('should go inside playFrame', () => {
            //Given
            let score = new Score();
            const playFrameSpy = sinon.spy(score, 'playFrame');
            let game = new Game(score);
            //When
            game.play();
            //Then
            sinon.assert.callCount(playFrameSpy, 20)
        })

        it('should fulfill the scoreboard of the two players', () => {
            //Given
            let score = new Score();
            let game = new Game(score);
            //When
            let result = game.play();
            //Then
            expect(game.scoreboard_player_one).to.be.instanceOf(Array).and.to.have.lengthOf(10);
            expect(game.scoreboard_player_two).to.be.instanceOf(Array).and.to.have.lengthOf(10);
        })

    })

})
