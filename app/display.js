class Display {

    constructor(){
    }

    displayScoreboard(scoreboard)
    {
        let result = '['
        for (let i = 0; i < scoreboard.length; i++)
        {
            result += '['
            result+= scoreboard[i][0] + ',' + scoreboard[i][1]
            result += ']'
            if (i+1 < scoreboard.length){
                result+= ','
            }
        }

        result += ']'
        //console.log(result)
        return result
    }

}

module.exports = Display
