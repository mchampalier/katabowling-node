const Score = require('../app/project.js');

class Game {

    score;
    scoreboard_player_one;
    scoreboard_player_two;

    constructor(score){
        this.score = score;
        this.scoreboard_player_one = [];
        this.scoreboard_player_two = [];
    }

    play()
    {
        for (let turn  = 0; turn < 10; turn++)
        {
            this.playTurn(this.scoreboard_player_one);
            this.playTurn(this.scoreboard_player_two);
        }
        return this.score.computeScore(this.scoreboard_player_one);
    }

    playTurn(scoreboard) {
        const framePlayer = this.score.playFrame();
        scoreboard.push(framePlayer);
    }
}

module.exports = Game
